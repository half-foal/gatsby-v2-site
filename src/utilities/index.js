import colors from './colors'
import animations from './animations'
export * from './colors'
export * from './animations'

export { colors, animations }
