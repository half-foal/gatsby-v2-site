export const white = '#fff'
export const black = 'rgb(4,4,4)'
export const champagne = '#f9e1d5'

export const calico = '#ddbe85'
export const space = '#1c1f1e'
export const cucumber = '#78b460'
export const buckthorn = '#f5a328'
export const pomegranate = '#f44336'

export const deepspace = '#232855'
export const orangey = '#ffd36b'

export const linen = '#f9f4eb'
export const tangaroa = '#041f33'

export const gradientBlack =
  'linear-gradient(to bottom, #1c1f1e, #181b1b, #151617, #111112, #0b0b0b)'

export default {
  white,
  black,
  champagne,
  calico,
  space,
  cucumber,
  buckthorn,
  pomegranate,
  gradientBlack,
  deepspace,
  orangey,
  linen,
  tangaroa,
}
