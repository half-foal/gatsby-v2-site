export const bezier = 'cubic-bezier(0.19, 1, 0.22, 1)'

export default {
  bezier,
}
