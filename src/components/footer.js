import React, { Component } from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'
import { colors } from '../utilities'

const FooterNav = styled.div`
  position: relative;
  background: ${colors.tangaroa};
  border-top: 1px solid grey;
  min-height: 30vh;
  color: ${colors.linen};
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  z-index: 4;
  padding: 1.5rem;
  a {
    color: ${colors.linen};
  }
  img {
    height: 50px;
  }
  nav {
    display: flex;
    flex-direction: column;
    ul {
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      margin: 0;
    }
    li {
      list-style-type: none;

      a {
        text-decoration: none;
      }
    }
    li:first-child {
      margin-right: 28px;
    }
  }
`

const FooterInfo = styled.div`
  max-width: 300px;
  @media (min-width: 800px) {
    max-width: 500px;
  }
  &:nth-child(2) {
    width: 300px;
  }
`

export default class Footer extends Component {
  constructor() {
    super()

    var today = new Date(),
      date = today.getFullYear()

    this.state = {
      date: date,
    }
  }

  render() {
    return (
      <FooterNav>
        <FooterInfo>
          <h6>&#169; {this.state.date} </h6>
          <p>Half Foal</p>
          <p>
            {/* Use of this site constitutes acceptance of our Terms of Use and Privacy Policy. The material on this site may not be reproduced, distributed, transmitted, cached or otherwise used, except with prior written permission of Half Foal. */}
          </p>
        </FooterInfo>

        <FooterInfo>
          <nav>
            <h6>Social</h6>
            <ul>
              <li>
                <a
                  href="https://gitlab.com/half-foal"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  my gitlab
                </a>
              </li>
              <li>
                <Link to="/contact">talk to me</Link>
              </li>
            </ul>
          </nav>
        </FooterInfo>
      </FooterNav>
    )
  }
}
