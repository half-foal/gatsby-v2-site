import React from 'react'
import styled from 'styled-components'
import { colors } from '../utilities'
import BackgroundImg from '../images/Run.jpg'

const Background = styled.div`
  background-image: url('${BackgroundImg}');
  background-color: rgba(249, 244, 235, 0.5);
  background-blend-mode: screen;
  background-size: cover;
  height: 100vh;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  h1 {
    text-align: center;
    color: ${colors.tangaroa};
  }
`

const Home = () => (
  <Background>
    <h1>half foal</h1>
  </Background>
)

export default Home
