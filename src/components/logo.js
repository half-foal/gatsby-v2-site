import React from 'react'
import styled, { keyframes } from 'styled-components'
import HorseHead from '../images/halffoal-logo-front.png'
import HorseButt from '../images/halffoal-logo-back.png'

const horsey_upright = keyframes`
  0% {
    transform: rotate(45deg);
  }
  5% {
    transform: rotate(0);
  }
  40% {
    transform: rotateX(0);
  }
  45% {
    transform: rotateY(180deg);
  }
  100% {
    transform: rotateY(180deg);
  }

`

const horsey_butt = keyframes`
  10% {
    transform: rotateX(0);
  }
  15% {
    transform: rotateX(180deg);
  }
  20% {
    transform: translate(0px, -12px) rotateX(180deg);
  }
  25% {
    transform: translate(0px, 0px) rotateX(180deg);
  }
  30% {
    transform: translate(0px, 0px) rotateX(180deg);
  }
  50% {
    transform: translate(0px, 0px) rotateX(180deg);
  }
  55% {
    transform: translate(0px, -12px) rotateX(180deg);
  }
  60% {
    transform: translate(0px, 0px) rotateX(180deg);
  }
  100% {
    transform: translate(0) rotateX(180deg);
  }
`

const horsey_head = keyframes`
  25% {
    transform: translate(0);
  }
  30% {
    transform: translate(0, -12px);
  }
  35% {
    transform: translate(0);
  }
  45% {
    transform: translate(0);
  }
  50% {
    transform: translate(0, -12px);
  }
  55% {
    transform: translate(0);
  }
`

const LogoContainer = styled.div`
  max-height: 60px;
  margin: 1rem 0 0 0;
  width: 60px;
  transform: rotate(45deg);
  &:hover {
    animation: 3s ${horsey_upright} forwards;
    .horse-head {
      animation: 3s ${horsey_head} forwards;
    }
    .horse-butt {
      animation: 3s ${horsey_butt} forwards;
    }
  }
  img {
    height: 60px;
    width: auto;
    position: relative;
  }
  .horse-head {
    top: -2px;
  }
  .horse-butt {
    margin: 0 4px;
  }
`

const Logo = () => (
  <LogoContainer>
    <img src={HorseButt} className="horse-butt" alt="horse butt" />
    <img src={HorseHead} className="horse-head" alt="horse head" />
  </LogoContainer>
)

export default Logo
