import React from 'react'
import styled from 'styled-components'
// import { Spring } from 'react-spring'

// import logo from '../images/gatsby-astronaut.png'

const FullScreenOverlay = styled.div`
  background: teal;
  top: 0;
  left: 0;
  width: 100%;
`

const Overlay = () => <FullScreenOverlay />

export default Overlay
