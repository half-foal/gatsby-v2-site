import React from 'react'
import styled from 'styled-components'
import { Link } from 'gatsby'
import { colors } from '../utilities'
import Logo from './logo'

const Nav = styled.nav`
  background-color: ${colors.linen};
  z-index: 5;
  padding: 1.5rem;
  margin: 0 auto;
  li {
    a {
      font-family: 'Eczar', serif;
      font-weight: 700;
      text-decoration: none;
      color: ${colors.tangaroa};
    }
  }

  @media (max-width: 720px) {
    padding: 1rem;
  }

  ul {
    margin: 0;
    list-style: none;
  }
  .nav-list {
    display: flex;
    align-items: center;
    justify-content: center;
    list-style: none;
    flex-wrap: wrap;
    .logo-item {
      display: flex;
      flex: 1;
      justify-content: center;
    }
    .nav-item {
      margin-left: 1rem;
      margin-right: 1rem;
      margin-bottom: 0;
    }
    .dummy-link {
      width: 61px;
      height: 30px;
    }

    .mobile-nav-list {
      height: 100%;
      width: 100%;
      display: flex;
      flex-direction: column;
      list-style: none;
      justify-content: center;

      a {
        color: ${colors.orangey};
      }
    }
    .mobile-menu-listing {
    }
    @media (min-width: 720px) {
      .mobile-menu-listing {
        display: none;
      }
    }
    @media (max-width: 719px) {
      .desktop-menu-listing {
        display: none;
      }
      .mobile-menu-listing {
        z-index: 3;
        margin-bottom: 3rem;
      }
      .logo-container {
        img {
          max-height: 45px;
        }
      }
    }
  }
`

const navigation = () => (
  <Nav>
    <ul className="nav-list">
      <li className="nav-item">
        <Link to="/about">About</Link>
      </li>
      <li className="nav-item dummy-link" />
      <li className="nav-item logo-item">
        <Link to="/">
          <Logo />
        </Link>
      </li>
      <li className="nav-item">
        <Link to="/projects">Projects</Link>
      </li>
      <li className="nav-item">
        <Link to="/contact">Contact</Link>
      </li>
    </ul>
  </Nav>
)
export default navigation
