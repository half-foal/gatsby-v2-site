import React from 'react'
import styled from 'styled-components'
import { colors } from '../utilities'

const HelloContainer = styled.section`
  position: relative;
  min-height: 100vh;
  padding: 3vh calc(3vh + 3vw);
  // background-image: linear-gradient(to bottom, #1c1f1e, #181b1b, #151617, #111112, #0b0b0b);
  background: ${colors.tangaroa};
  display: flex;
  flex-direction: column;
  h4 {
    color: ${colors.linen};
  }
  a {
    color: ${colors.white};
  }
  .hello-content {
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    max-width: 1129px;
  }
`

const Introduction = () => (
  <HelloContainer>
    <div className="hello-content">
      <h4>Hello-I'm Patrick, a front-end web developer from Cleveland, OH.</h4>
      <br />
      <h4>This is my portfolio.</h4>
    </div>
  </HelloContainer>
)

export default Introduction
