import React from 'react'
import { StaticQuery, graphql, Link } from 'gatsby'

const RECENT_POST_QUERY = graphql`
  query RecentPostFromArchive {
    allMarkdownRemark(
      limit: 1
      sort: { order: DESC, fields: [frontmatter___date] }
    ) {
      edges {
        node {
          frontmatter {
            title
            slug
          }
        }
      }
    }
  }
`

const RecentPost = () => (
  <StaticQuery
    query={RECENT_POST_QUERY}
    render={({ allMarkdownRemark }) => (
      <>
        <aside>
          <h3>Recent Post</h3>
          <ul>
            {allMarkdownRemark.edges.map(edge => (
              <li key={edge.node.frontmatter.slug}>
                <Link to={`/posts/${edge.node.frontmatter.slug}`}>
                  {edge.node.frontmatter.title}
                </Link>
              </li>
            ))}
          </ul>
        </aside>
      </>
    )}
  />
)

export default RecentPost
