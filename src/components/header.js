import React, { Component } from 'react'
import styled from 'styled-components'
import { animations } from '../utilities'
// import logo from '../images/gatsby-astronaut.png'
import Navigation from './navigation'

const HeaderWrapper = styled.div`
  img {
    margin-bottom: 0;
  }
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  transition: transform 0.5s ${animations.bezier};
`

const HeaderContainer = styled.div`
  margin: 0 auto;
  maxwidth: 960;
`

export default class Header extends Component {
  constructor(props) {
    super(props)

    this.state = {
      height: 0,
      auth: false,
      slide: 0,
      visible: 1,
      lastScrollY: 0,
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll)

    const height = this.headerElem.clientHeight
    this.setState({ height })
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleScroll = () => {
    const { lastScrollY } = this.state
    const currentScrollY = window.scrollY

    if (currentScrollY > lastScrollY) {
      this.setState({ slide: '-108px', visible: 0 })
    } else {
      this.setState({ slide: '0px', visible: 1 })
    }
    this.setState({ lastScrollY: currentScrollY })
  }
  render() {
    return (
      <HeaderWrapper
        ref={headerElem => (this.headerElem = headerElem)}
        style={{
          opacity: `${this.state.visible}`,
          transform: `translate(0, ${this.state.slide})`,
          transition: `transform  ${animations.bezier}s, opacity 0.4s`,
        }}
      >
        <HeaderContainer>
          <Navigation />
        </HeaderContainer>
      </HeaderWrapper>
    )
  }
}
