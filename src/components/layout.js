import React from 'react'
import PropTypes from 'prop-types'
// import Img from 'gatsby-image'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'
import styled from 'styled-components'
import { Spring } from 'react-spring'

import Header from './header'
import Footer from './footer'
import Archive from './archive'
import RecentPost from './recentPost'
import Home from './home'
import Introduction from './introduction'

import './layout.css'
import './fonts.css'
import { colors } from '../utilities'
import MobileNav from './mobileNav'
import Logo from './logo'

// import ReactAnimTest from './ReactAnimTest'
// import Overlay from './overlay'

const MainLayout = styled.main`
  min-height: 100vh;
  background-color: ${colors.linen};
  padding-bottom: 1rem;
  position: relative;
  top: 100px;
`

const Layout = ({ children, location }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
            description
            keywords
          }
        }
        file(relativePath: { regex: "/bg/" }) {
          childImageSharp {
            fluid(maxWidth: 1000) {
              ...GatsbyImageSharpFluid_tracedSVG
            }
          }
        }
      }
    `}
    render={data => (
      <>
        <Helmet
          title={data.site.siteMetadata.title}
          meta={[
            {
              name: 'description',
              content: data.site.siteMetadata.description,
            },
            { name: 'keywords', content: data.site.siteMetadata.keywords },
          ]}
        >
          <html lang="en" />
        </Helmet>
        <Spring
          from={{ opacity: 0 }}
          to={{ opacity: 1 }}
          leave={{ opacity: 0 }}
        >
          {props => (
            <div style={props}>
              {/* <MobileNav /> */}
              <Header siteTitle={data.site.siteMetadata.title} />
              <MainLayout>
                {location.pathname === '/' && <Home />}
                {location.pathname === '/' && <Introduction />}
                <div>{children}</div>
                {/* <Archive /> */}
                <RecentPost />
              </MainLayout>
              <Footer />
            </div>
          )}
        </Spring>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

Layout.defaultProps = {
  location: {},
}

export default Layout
