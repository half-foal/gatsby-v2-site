import React, { Component } from 'react'
import { Spring } from 'react-spring'

export default class ReactAnimTest extends Component {
  render() {
    return (
      <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
        {props => <div style={props}>hello</div>}
      </Spring>
    )
  }
}
