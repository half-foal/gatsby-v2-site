import React from 'react'
import Layout from '../components/layout'

const Contact = () => (
  <Layout>
    <form
      name="contactform-3"
      method="POST"
      netlify-honeypot="bot-field"
      netlify
    >
      <input className="hidden" name="bot-field" type="hidden" />
      <p>
        <label for="name">Your Name:</label>
        <input type="text" name="name" id="name" required />
      </p>
      <p>
        <label for="email">Your Email:</label>
        <input type="email" name="email" id="email" required />
      </p>
      <p>
        <label for="message">Message:</label>
        <textarea name="message" id="message" required />
      </p>
      <div netlify-recaptcha />
      <p>
        <button type="submit">Send</button>
      </p>
    </form>
  </Layout>
)

export default Contact
