import React from 'react'
import Layout from '../components/layout'

const About = ({ location }) => (
  <Layout location={location}>
    <h1>This is about us</h1>
  </Layout>
)

export default About
