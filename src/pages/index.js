import React from 'react'

import Layout from '../components/layout'
// import Listing from '../components/listing'
import RecentPost from '../components/recentPost'

const IndexPage = ({ location }) => (
  <Layout location={location}>
    <RecentPost />
  </Layout>
)

export default IndexPage
