import React from 'react'
import Layout from '../components/layout'

const Thanks = () => (
  <Layout>
    <h1>Thank you</h1>
  </Layout>
)

export default Thanks
